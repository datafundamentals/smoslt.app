package smoslt.app;

import java.util.List;

import smoslt.analytics.ScoreSpreadsheetWriter;
import smoslt.analytics.SpreadsheetConfiguration;
import smoslt.analytics.SpreadsheetConfigurationFactory;
import smoslt.given.ParseDelimitedFile;

public class Weigh {

	public void exec() {
		SpreadsheetConfiguration spreadsheetConfiguration = SpreadsheetConfigurationFactory
				.getInstance().get(null, "yada");
		ParseDelimitedFile parseDelimitedFile = new ParseDelimitedFile();
		List<List<String>> fieldValuesLists = parseDelimitedFile
				.get(SpreadsheetConfiguration.getCsvOutputFilePath());
		new ScoreSpreadsheetWriter(spreadsheetConfiguration, fieldValuesLists)
				.go();
	}

}
