/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.app module, one of many modules that belongs to smoslt

 smoslt.app is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.app is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.app in the file smoslt.app/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.app;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import org.btrg.uti.LoadProperties_;
import org.btrg.uti.OrderFileByAlphaLines_;
import org.btrg.uti.TestTimer_;
import org.btrg.uti.WriteProperties_;

import smoslt.analytics.ScoreKeeperImpl;
import smoslt.analytics.ScoreSpreadsheetWriter;
import smoslt.analytics.SpreadsheetConfiguration;
import smoslt.analytics.SpreadsheetConfigurationFactory;
import smoslt.domain.FileConstants;
import smoslt.domain.Schedule;
import smoslt.domain.ScheduleBuild;
import smoslt.given.ParseDelimitedFile;
import smoslt.given.SideEffectListImpl;
import smoslt.mprtxprt.ExportIntoProjectLibre;
import smoslt.mprtxprt.ImportFromProjectLibre;
import smoslt.options.OptionsSolution;
import smoslt.options.Solve;
import smoslt.scoremunger.Munge;
import smoslt.stacker.StackTasks;
import smoslt.util.ConfigProperties;

public class Launch {
	Properties properties;

	public void go(File inputFile, List<String> sideEffects) {
		properties = ConfigProperties.get();
		TestTimer_ testTimer = new TestTimer_("Scoring and Scheduling run for "+ properties.getProperty(ConfigProperties.PROJECT_NAME));
		File generateDirectory = createGenerateDir();
		File exportFile = new File(generateDirectory, properties.getProperty(ConfigProperties.PROJECT_NAME)
				+ ".pod");
		ScheduleBuild scheduleBuild = new ImportFromProjectLibre(inputFile);
		Schedule schedule = new Schedule(scheduleBuild);
		ScoreKeeperImpl scoreKeeper = runTasks(schedule);
		OptionsSolution optionsSolution = new OptionsSolution();
		optionsSolution.setScoreKeeper(scoreKeeper);
		optionsSolution.setOptionNameList(sideEffects(sideEffects));
		// System.out.println("\n\nTRYING COUNT AT:");
		OptionsSolution bestOptionsSolution = (OptionsSolution) new Solve()
				.go(optionsSolution);
		// String log = "completed at "+StringUtils_.yyMMddHHmmss(new Date())+
		// "\n";
		// NioFileUtil.append(OUTPUT_PATH, log.getBytes());
		System.out.println("BEST WAS " + bestOptionsSolution.getScore());
		OrderFileByAlphaLines_.instance().order(
				properties.getProperty(ConfigProperties.GENERATE_FOLDER) + "/"
						+ properties.getProperty(ConfigProperties.CSV_OUTPUT_DATA_FILE_NAME));
		ExportIntoProjectLibre exportIntoProjectLibre = new ExportIntoProjectLibre(
				scoreKeeper.getScheduleClone());
		exportIntoProjectLibre.go();
		exportIntoProjectLibre.export(exportFile);
		new Munge().exec();
		SpreadsheetConfiguration spreadsheetConfiguration = SpreadsheetConfigurationFactory
				.getInstance().get(schedule, "yada");
		ParseDelimitedFile parseDelimitedFile = new ParseDelimitedFile();
		List<List<String>> fieldValuesLists = parseDelimitedFile
				.get(SpreadsheetConfiguration.getCsvOutputFilePath());
		new ScoreSpreadsheetWriter(spreadsheetConfiguration, fieldValuesLists)
				.go();
		System.out.println("\n");
		testTimer.doneMinutes();
		System.out.println("\n");
	}


	private File createGenerateDir() {
		File generateDir = new File(properties.getProperty(ConfigProperties.GENERATE_FOLDER));
		if(!generateDir.exists()){
			generateDir.mkdirs();
		}
		return generateDir;
	}


	List<String> sideEffects(List<String> sideEffects) {
		List<String> sideEffectsToRunWith;
		if (null != sideEffects) {
			sideEffectsToRunWith = sideEffects;
		} else {
			sideEffectsToRunWith = new SideEffectListImpl().get();
		}
		return sideEffectsToRunWith;
	}

	ScoreKeeperImpl runTasks(Schedule schedule) {
		ScoreKeeperImpl scoreKeeper = new ScoreKeeperImpl(new StackTasks(),
				schedule);
		return scoreKeeper;
	}

}
